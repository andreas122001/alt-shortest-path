import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Initialiserer...");
        Graph g = new Graph("noder.txt","kanter.txt","interessepkt.txt");
        System.out.println("\nVeifinner v1.0\n");
        Scanner sc = new Scanner(System.in);
        boolean run = true;
        while (run) {
            System.out.println("Hva vil du gjøre?\n" +
                    "1. Finn raskeste vei\n" +
                    "2. Finn 10 nærmeste bensinstasjon/ladestasjon\n" +
                    "3. Avslutt");
            String in = sc.nextLine();
            if (in.equals("1")) {
                System.out.println("Hvor vil du reise fra (stedsnavn)?");
                String start = sc.nextLine();
                Graph.Node startNode = g.findByName(start);
                if (startNode == null) {
                    System.out.println("Finner ikke '"+start+"', prøv på nytt");
                    continue;
                }
                System.out.println("Hvor vil du reise til (stedsnavn)?");
                String stop = sc.nextLine();
                Graph.Node stopNode = g.findByName(stop);
                if (stopNode == null) {
                    System.out.println("Finner ikke '"+stop+"', prøv på nytt");
                    continue;
                }
                String[] djikRes = g.runDjikstras(startNode,stopNode);
                String[] altRes = g.runALT(startNode,stopNode);

                System.out.println("Djikstras tok "+djikRes[0]+" ms og gikk gjennom "+djikRes[1]
                        + " noder med reisetid "+djikRes[2]+" (hh:mm:ss)");
                System.out.println("ALT tok "+altRes[0]+" ms og gikk gjennom "+altRes[1]
                        +" noder med reisetid "+altRes[2]+" (hh:mm:ss)");

                System.out.println("Vil du eksportere resultatene?\n" +
                        "1. Ja\n" +
                        "2. Nei");
                in = sc.nextLine();
                if (!in.equals("1")) continue;

                System.out.println("Hvor vil du eksportere resultatene?");
                File file = new File(sc.nextLine());
                try {
                    g.exportPath(stopNode,file);
                } catch (IOException e) {
                    System.err.println("Problem med å eksportere: "+e.getMessage());
                }
            } else if (in.equals("2")) {
                System.out.println("Hvor vil du søke fra?");
                String start = sc.nextLine();
                Graph.Node startNode = g.findByName(start);
                if (startNode == null) {
                    System.out.println("Finner ikke '"+start+"', prøv på nytt");
                    continue;
                }
                System.out.println("Hva vil du finne?\n" +
                        "1. Bensinstasjon\n" +
                        "2. Ladestasjon");
                in = sc.nextLine();
                int type = -1;
                if (in.equals("1")) type = 2;
                else if (in.equals("2")) type = 4;

                Graph.Node[] res = g.findClosest(startNode,type,10);
                for (Graph.Node n : res) {
                    System.out.println(n.name + ", " + n.getTime()+" unna");
                }

            } else if (in.equals("3")) {
                run = false;
            } else {
                System.err.println("'"+in+"' ikke gyldig svar...");
            }
            System.out.println();
        }
        sc.close();
    }
}









