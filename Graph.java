import java.io.*;
import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.StringTokenizer;


public class Graph {

    private int numNodes, numEdges;
    private Node[] nodes, reverseNodes;
    private final int[] landmarkIds = new int[]{3919068, 5116554, 6431068};
                                    // Skarsvåg, Helsingfors, krsitiansand
    private final Node[] landmarks = new Node[landmarkIds.length];
    private int[][] fromLMs, toLMs;

    private void readFile(String nodeFile, String edgeFile, String nameFile) throws IOException {
        // Read nodes
        BufferedReader nodeReader = new BufferedReader(new FileReader(nodeFile));
        StringTokenizer nodeToken = new StringTokenizer(nodeReader.readLine());
        numNodes = Integer.parseInt(nodeToken.nextToken());
        nodes = new Node[numNodes];
        for (int i = 0; i < numNodes; i++) {
            nodeToken = new StringTokenizer(nodeReader.readLine());
            int node = Integer.parseInt(nodeToken.nextToken());
            double lat = Double.parseDouble(nodeToken.nextToken());
            double lon = Double.parseDouble(nodeToken.nextToken());
            nodes[node] = new Node(node, lat, lon);
        }
        nodeReader.close();

        // Read edges
        BufferedReader edgeReader = new BufferedReader(new FileReader(edgeFile));
        StringTokenizer edgeToken = new StringTokenizer(edgeReader.readLine());
        numEdges = Integer.parseInt(edgeToken.nextToken());
        for (int i = 0; i < numEdges; i++) {
            edgeToken = new StringTokenizer(edgeReader.readLine());
            int fromNode = Integer.parseInt(edgeToken.nextToken());
            int toNode = Integer.parseInt(edgeToken.nextToken());
            int weight = Integer.parseInt(edgeToken.nextToken());
            nodes[fromNode].edgeTo1 = new Edge(nodes[toNode], nodes[fromNode].edgeTo1, weight);
        }
        edgeReader.close();

        // Read name and type of nodes
        BufferedReader reader = new BufferedReader(new FileReader(nameFile));
        StringTokenizer tok = new StringTokenizer(reader.readLine());
        int numEntries = Integer.parseInt(tok.nextToken());
        for (int i = 0; i < numEntries; i++) {
            tok = new StringTokenizer(reader.readLine());
            int index = Integer.parseInt(tok.nextToken());
            nodes[index].type = Integer.parseInt(tok.nextToken());
            tok.nextToken("\"");
            nodes[index].name = tok.nextToken("\"");
        }
        reader.close();
    }

    public Graph(String file1, String file2, String file3) {
        try {
            readFile(file1,file2,file3);
        } catch (IOException e) {
            System.err.println("Mangler filer: '"+file1+"' '"+file2+"' '"+file3+"'");
        }
        initReverseGraph();
        for (int i = 0; i < landmarkIds.length; i++) landmarks[i] = getNode(landmarkIds[i]);
        initALT();
    }

    public Node[] findClosest(Node start, int type, int amount) {
        Node[] res = new Node[amount];
        for (int i = 0; i < amount; i++) {
            Node n = djikstras2(start,type,nodes);
            res[i] = n;
            n.type = -1;
        }
        for (Node n : res) n.type = type;
        return res;
    }

    public void initALT() {
        try {
            preProcess();
            readPreProcessedData();
        } catch (IOException e) {
            System.err.println("Problem with pre-processing data: "+e.getMessage());
        }
    }

    public Node findByName(String name) {
        for (Node n : nodes) {
            if (name.equalsIgnoreCase(n.name)) return n;
        }
        return null;
    }

    public void exportPath(Node node, File file) throws IOException {
        PrintWriter pw = new PrintWriter(new FileWriter(file));

        while(node != null) {
            pw.write(node.lat + "," + node.lon + "\n");
            node = node.parent;
        }
        pw.flush(); pw.close();
    }

    public Node[] getAllFrom(Node n) {
        int size = 0;
        for (Edge e = n.edgeTo1; e != null; e = e.next) {
            size++;
        }
        Node[] out = new Node[size];
        Edge e = n.edgeTo1;
        for (int i = 0; i < size; i++) {
            out[i] = e.to;
            e = e.next;
        }
        return out;
    }

    public Node getNodeReverse(int i) {
        return reverseNodes[i];
    }

    public Node getNode(int i) {
        return nodes[i];
    }

    private void resetGraph() {
        for (Node n : nodes) {
            n.distance = -1;
            n.approxLen = -1;
            n.parent = null;
        }
    }

    public String[] runALT(Node startNode, Node stopNode) {
        double time0 = System.currentTimeMillis();
        int numNodes = alt(startNode, stopNode);
        double time1 = System.currentTimeMillis();
        return new String[]{String.valueOf(time1-time0), String.valueOf(numNodes), stopNode.getTime()};
    }

    public String[] runDjikstras(Node startNode, Node stopNode) {
        double time0 = System.currentTimeMillis();
        int numNodes = djikstras(startNode,stopNode,nodes);
        double time1 = System.currentTimeMillis();
        return new String[]{String.valueOf(time1-time0), String.valueOf(numNodes), stopNode.getTime()};
    }

    /**
     * FIX
     * @param startNode
     * @param stopNode
     */
    private int alt(Node startNode, Node stopNode) {
        resetGraph();
        PriorityQueue<Node> pq =
                new PriorityQueue<>(nodes.length-1,
                        Comparator.comparingInt(Node::sumLen));
        // initialize:
        startNode.approxLen = getBestApprox(startNode,stopNode);
        startNode.distance = 0;
        pq.add(startNode);

        int nodesProcessed = 0;

        // loop:
        while(!pq.isEmpty()) {
            // delta(n,end) >= delta(LM,end)-delta
            Node current = pq.poll();
            nodesProcessed++;
            if (current == stopNode) break;
            for (Edge e = current.edgeTo1; e!=null; e=e.next) {
                if (e.to.distance == -1) {
                    // Calculate distances:
                    e.to.distance = current.distance + e.weight;
                    e.to.approxLen = getBestApprox(e.to, stopNode);;
                    e.to.parent = current;
                    pq.add(e.to);
                } else if (e.to.distance > current.distance + e.weight) {
                    // update priority/distance:
                    updatePrio(e.to, current,current.distance + e.weight, pq);
                }
            }
        }
        return nodesProcessed;
    }

    private int getBestApprox(Node n, Node stopNode) {
        int approx = 0;
        for (int i = 0; i < landmarks.length; i++) {
            int diff = fromLMs[i][stopNode.id] - fromLMs[i][n.id];
            int diff2 = toLMs[i][n.id] - toLMs[i][stopNode.id];
            approx = Math.max(approx, Math.max( Math.max(diff,0),diff2));
        }
        return approx;
    }

    /**
     * Test?
     * @param n
     * @param distance
     * @param pq
     */
    public void updatePrio(Node n, Node current, int distance, PriorityQueue<Node> pq) {
        boolean rm = pq.remove(n);
        n.distance = distance;
        n.parent = current;
        if (rm) pq.add(n);
    }

    public void djikstras(Node startNode) {
        djikstras(startNode,null,nodes);
    }

    public int djikstras(Node startNode, Node stopNode, Node[] graph) {
        resetGraph();
        PriorityQueue<Node> queue = new PriorityQueue<>(graph.length-1,Comparator.comparingInt(n -> n.distance));
        startNode.distance = 0;
        queue.add(startNode);
        int nodesProcessed = 0;

        while (!queue.isEmpty()) {
            Node current = queue.poll();
            nodesProcessed++;
            if (current == stopNode) break;
            for (Edge e = current.edgeTo1; e != null; e = e.next) {
                if (e.to.distance == -1) {
                    e.to.distance = current.distance + e.weight;
                    e.to.parent = current;
                    queue.add(e.to);
                }
                else if (e.to.distance > current.distance + e.weight) {
                    e.to.distance = current.distance + e.weight;
                    e.to.parent = current;
                }
            }
        }
        return nodesProcessed;
    }

    public Node djikstras2(Node start, int type, Node[] graph) {
        resetGraph();
        PriorityQueue<Node> queue = new PriorityQueue<>(graph.length-1,Comparator.comparingInt(n -> n.distance));
        start.distance = 0;
        queue.add(start);

        while (!queue.isEmpty()) {
            Node current = queue.poll();
            if (current.type == type) return current;
            for (Edge e = current.edgeTo1; e != null; e = e.next) {
                if (e.to.distance == -1) {
                    e.to.distance = current.distance + e.weight;
                    e.to.parent = current;
                    queue.add(e.to);
                }
                else if (e.to.distance > current.distance + e.weight) {
                    e.to.distance = current.distance + e.weight;
                    e.to.parent = current;
                }
            }
        }
        return null;
    }

    public void initReverseGraph() {
        reverseNodes = new Node[nodes.length];
        for (int i = 0; i < nodes.length; i++) {
            reverseNodes[i] = new Node(nodes[i].id,nodes[i].lat,nodes[i].lon);
        }
        for (int i = 0; i < nodes.length; i++) { // for each node
            for (Edge e = nodes[i].edgeTo1; e!=null; e=e.next) { // for each edge from node
                // give to-node new edge with to to node n (t-t-tch)
                reverseNodes[e.to.id].edgeTo1 =
                        new Edge(nodes[i],reverseNodes[e.to.id].edgeTo1,e.weight);
            }
        }
    }

    /**
     * Done
     */
    public void readPreProcessedData() throws IOException {
        System.out.println("Reading pre-processed data...");
        File preProcFile = new File("ppf.ppfx");
        BufferedReader in = new BufferedReader(new FileReader(preProcFile));
        int numLM = Integer.parseInt(in.readLine());

        fromLMs = new int[numLM][nodes.length];
        for (int i = 0; i < numLM; i++) {
            for (int j = 0; j < nodes.length; j++) {
                fromLMs[i][j] = Integer.parseInt(in.readLine());
            }
        }

        toLMs = new int[numLM][nodes.length];
        for (int i = 0; i < numLM; i++) {
            for (int j = 0; j < nodes.length; j++) {
                toLMs[i][j] = Integer.parseInt(in.readLine());
            }
        }
        System.out.println("Pre-processed data read!");
    }

    /**
     * Maybe done?
     */
    public void preProcess() throws IOException {
        File preProcFile = new File("ppf.ppfx");
        // numLandmarks [distToLMx] [distFromLMx]
        if (!preProcFile.exists()) {
            System.out.println("Pre-processing needed.\nPre-processing...");
            BufferedWriter out = new BufferedWriter(new FileWriter(preProcFile));
            out.write(landmarks.length+"\n"); // writeNumLM
            // Write distance from landmarks
            System.out.println("Finding to LM...");
            for (Node landmark : landmarks) {
                System.out.println("    Processing: " + landmark);
                djikstras(landmark);
                for (Node n : nodes) {
                    out.write(n.distance+"\n");
                    if (n.id % 10000 == 0) System.out.println("       Node: " + n);
                }
            }
            // Write distance to landmarks (reverse graph)
            System.out.println("Finding from LM...");
            for (Node landmark : landmarks) {
                System.out.println("    Processing: " + landmark);
                djikstras(landmark,null, reverseNodes);
                for (Node n : reverseNodes) {
                    out.write(n.distance+"\n");
                    if (n.id % 10000 == 0) System.out.println("       Node: " + n);
                }
            }
            out.close();
            System.out.println("Pre-processing done!\n");
        }
    }












    static class Node {
        int id, distance, approxLen;
        Edge edgeTo1;
        Node parent;
        double lat, lon;
        String name;
        int type;

        Node(int id, double lat, double lon) {
            this.id = id;
            this.lat = lat;
            this.lon = lon;
            this.approxLen = -1;
            this.distance = -1;
            this.parent = null;
        }

        public String toString() {
            if (name!=null) return "("+name+" "+id+")";
            return "("+id+" "+lat+" "+lon+")";
        }

        String getTime() {
            return (distance / 360000)+":"+(distance / 6000)%60+":"+((distance/100)%60);
        }

        int sumLen() {
            return distance + approxLen;
        }
    }

    static class Edge {
        Edge next;
        Node to;
        int weight;
        Edge(Node to, Edge next, int weight) {
            this.to = to;
            this.next = next;
            this.weight = weight;
        }
    }
}
